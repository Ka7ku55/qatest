const { Before } = require('cypress-cucumber-preprocessor/steps');

Before(() => {
  cy.visit('/');
});

const user = {};

given('the user has the correct credentials', () => {
  user.name = 'test@drugdev.com';
  user.password = 'supers3cret';
});

given('the user has the incorrect credentials', () => {
  user.name = 'wrongname@test.com';
  user.password = 'wrongpassword';
});

when('the user enters username', () => {
  cy.get('[name=email]').type(user.name);
});

then('the user enters password', () => {
  cy.get('[name=password]').type(user.password);
});

then('clicks Login', () => {
  cy.get('button[aria-label=login]').click();
});

then('the user is presented with a welcome message', () => {
  cy.contains('Welcome Dr I Test').should('be.visible');
});

then('the user is presented with a error message', () => {
  cy.get('#login-error-box').should('be.visible');
});
